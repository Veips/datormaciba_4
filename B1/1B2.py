# Fails: 1B2.py
# Autors: Rihards Veips
# Apliecibas numurs: 193NEB006

from numpy import * # matematikas funkcijas
from matplotlib import pyplot as plt # grafiskais zimetajs

x = linspace(0, 7, 70)
y = sin(x)

plt.grid()
plt.xlabel('x')
plt.ylabel('f(x)')
plt.title('$sin(x)$')
plt.plot(x, y, color = "#0099FF")

y = x
plt.plot(x, y)
y = x - pow(x,2)/math.factorial(3)
plt.plot(x, y)
y = x - pow(x,2)/math.factorial(3)+pow(x,5)/math.factorial(5)
plt.plot(x, y)
y = x - pow(x,2)/math.factorial(3)+pow(x,5)/math.factorial(5)-pow(x,7)/math.factorial(7)
plt.plot(x, y)
plt.show()
