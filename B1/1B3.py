# Fails: 1B3.py
# Autors: Rihards Veips
# Apliecibas numurs: 193NEB006

from matplotlib.pyplot import figure, show
from math import # pi konstante

fig = figure()

ax = fig.add_subplot(111, polar=True) # polaras koordinates grafiks
theta = [30,60] # gradi
theta = [i*pi/180 for i in theta] # gradi radianos
r = [1.,2.] # radius
ax.bar(theta,r, width=0.01) # radiusu zime ar platumu 0.01
show() # atver zimejumu
