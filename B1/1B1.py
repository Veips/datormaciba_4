# Fails: 1B1.py
# Autors: Rihards Veips
# Apliecibas numurs: 193NEB006

from numpy import * # matematikas funkcijas
from matplotlib import pyplot as plt # grafiskais zimetajs

x = linspace(0, 7, 70)
y = cos(x)
y2 = sin(x)

plt.grid()
plt.xlabel('x')
plt.ylabel('f(x)')
plt.title('$cos(x)$, $sin(x)$')
plt.plot(x, y, color = "#0088FF")
plt.plot(x,y2)
plt.show()
