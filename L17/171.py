# Fails : 171.py
# Autors : Rihards Veips
# Apliecibas numurs : 193NEB006

# -*- coding : utf -8 -*-
from math import sin , fabs
from time import sleep

def f(x):
    return 4*(x*x) + 8 * x - 16

# Definejam argumenta x robezhas :
a = float(input("a = "))
b = float(input("b = "))

# Aprekjinam funkcijas vertibas dotajos punktos :
funa = f(a)
funb = f(b)

# Paarbaudam , vai dotajaa intervaalaa ir saknes :
if ( funa * funb > 0.0 ):
    print("Dotajaa intervaalaa [%s, %s] saknju nav"%(a,b))
    sleep(1); exit() # Zinjo uz ekraana , gaida 1 sec . un darbu pabeidz
else:
    print("Dotajaa intervaalaa sakne(s) ir!")

# Defineejam precizitaati , ar kaadu mekleesim sakni :
deltax = 0.0001

# Sashaurinam saknes mekleeshanas robezhas :
iterations = 0
while ( fabs(b-a) > deltax ):
    x = (a+b)/2; funx = f(x)
    iterations = iterations + 1
    if ( funa*funx < 0. ):
        b = x
    else:
        a = x

print("Intervals [%s, %s]"%(a, b))
print("Delta x = +- %s"%(deltax))
print("f = 4x^2 + 8x - 16")
print("f(x) = 0")
print("x = %.4f"%(x))
print("f(%.4f) = %.4f"%(x, f(x)))
print("Iteraciju skaits: %s"%(iterations))