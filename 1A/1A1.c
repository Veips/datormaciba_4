// 1A0.c Programma funkcijas tuvinātās vērtības aprēķinam summējot skaitļu virknes elementus un lietojot rekurences izteiksmi
// Funkcija exp(x)
// Autors: Rihards Veips 193NEB006

#include <stdio.h>
#include <math.h>

#define N 8 // Definē konstanti N - summas locekļu skaits

int main(){

	int k; // Summas mainīgais
	double sum; // Sākuma summa
	double a = 1; // Summas tekošais loceklis, šoreiz pirmais
	double x = 1; // Izvēlēts funkcijas arguments
	sum = a; // Summa ir pirmā locekļa vērtība
	// Izdrukā sākuma vērtības
	printf ("N = %i, Sākuma summa = %g \n", N, sum);

	// Sāk ciklisku summēšanu
	for (k = 1; k<=N; k++ ){
		a = a * x/k;
		sum = sum + a;
		printf("[%d] summas loceklis ir: %f \n", k, a);
	}

	// Aprēķina y ar bibliotēkas funkciju:
	double y = exp(x);
	// Iegūtos datus izvada uz ekrāna
	printf("x = %g, y = %g, Summa = %g \n", x, y, sum);
}