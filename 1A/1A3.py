# Fails: 1A2.py
# Autors: Rihards Veips
# Apliecibas numurs: 193NEB006

import math

N = 8
a = 1
x = 1

summa = a 

print("N =",N,", Sākuma summa =",summa)

k = 1
while(k<=N):
	a = a * x/k
	summa = summa + a
	print("[%s] summas loceklis ir %.4f"%(k, round(a, 5)))
	k = k + 1

y = math.exp(x)

print("x = %.4f, y = %.4f, Summa = %.4f"%(round(x, 5), round(y, 5), round(summa, 5)))