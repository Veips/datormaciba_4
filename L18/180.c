// Fails : 180.c
// Autors : Rihards Veips
// Apliecības numurs : 193NEB006


#include <stdio.h>
#include <math.h>

double f1(double x)
{
	return x * 2;
}

int main()
{
	// k - iterācijas numurs pēc kārtas
	int k;	// a, b – argumentu intervāls
	double a = -2, b = 2, I1 = 0., I2, h;	// I1 – iepriekšējais Integrālis,
	double eps = 0.001;	// I2 – tekošais Integrālis, kur 'eps' ir rezultāta
    int n = 1;
	// precizitāte angliski saukta "varepsilon"
	I2 = (b - a) *(f1(a) + f1(b)) / 2;	// Tādā veidā aprēķina integrāļa tuvinājumu
	while (fabs(I2 - I1) > eps)	// Tādā veidā, salīdzinot funkcijas summas tekošās
	{
		// vērtības precizitāti ar pieļaujamo eps vērtību,
		n = n * 2;	// palielina intervālu skaitu 2 reizes,
		h = (b - a) / n;	// nosaka intervāla platuma vērtību,
		I1 = I2;	// saglabā tekošo vērtību mainīgajā I1
		I2 = 0;	// Tekošā summas (integrāļa) vērtība
		// tiek nonullēta
		for (k = 0; k < n; k++)
			I2 = I2 + h* f1(a + (k + .5) *h);	// Summa no visiem laukumiņiem dod integrāļa
	}	// tuvināto vērtību
        
        printf("f(x) = 2 * x\n");
        printf("Argumentu vertibas: a = %.4f, b = %.4f)\n", a, b);
        printf("Noteikta integrala vertiba I = %.4f\n", I2);
        printf("Taisnsturu skaits N = %d pēc argumentu intervāla dalīšanas N daļās\n", n);
	// integrāļu vērtības ir tik tuvas, ka ir mazākas
	// par mainīgā 'eps' vērtību.
}

// salidzinot ar Maximu viss ir pareizi