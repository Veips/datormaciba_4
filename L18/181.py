# Fails : 181.py
# Autors : Rihards Veips
# Apliecības numurs : 193NEB006

from math import sin, fabs, sqrt
from time import sleep

def f1(x):
	return 2 * x

# Definējam iterācijas kārtas skaitli
k = ...
# Definējam argumenta x robežas a, b:
a = float(input("Argumenta intervala apakseja robeza = "))
b = float(input("Argumenta intervala augseja robeza = "))
I1 = 0.
# Python nav jādeklarē mainīgie pirms to vērtības definēšanas 
# I2 un h definēsim darba laikā

# Aprēķinu precizitāte
eps = 0.0001
n = 1
# Aprēķina pirmo integrāļa vērtības tuvinājumu
I2 = (b-a) * (f1(a) + f1(b)) / 2

while(fabs(I2-I1)>eps):
	n=n*2
	h=(b-a)/n
	I1=I2
	I2=0
	k=0
	while(k<n):
		I2 = I2 + h * f1(a + (k + .5) * h)
		k = k + 1

print("f(x) = 2 * x")
print("Argumentu vertibas: a = %.4f, b = %.4f)"%(a, b))
print("Noteikta integrala vertiba I = %.4f"%(I2))
print("Taisnsturu skaits N = %s pēc argumentu intervāla dalīšanas N daļās"%(n))

# salidzinot ar Maximu viss ir pareizi